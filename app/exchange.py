import ccxt
from ccxt.base.errors import RequestTimeout
import pandas as pd


class Exchange:

    def __init__(self, exchange):

        # This should be a try except

        if exchange:
            self.exchange = getattr(ccxt, exchange)({'enableRateLimit': True})

        else:
            print('Unable to load exchage.')

    def getCoins_BTC(self):
        return [coin for coin in self.exchange.fetch_tickers().keys() if '/BTC' in coin]

    def getCoins_USD(self):
        return [coin for coin in self.exchange.fetch_tickers().keys() if '/USD' in coin or '/USDT' in coin]

    def getCoins_ETH(self):
        return [coin for coin in self.exchange.fetch_tickers().keys() if '/ETH' in coin]

    def getTickData(self, coinPair):

        columns = ['date', 'open', 'high', 'low', 'close', 'volume']

        data = pd.DataFrame(self.exchange.fetch_ohlcv(
            coinPair, timeframe='1h'), columns=columns)

        data['date'] = pd.to_datetime(data['date'], unit='ms')
        data.set_index('date', inplace=True)

        ohlc_dict = {'open': 'first', 'high': 'max',
                     'low': 'min', 'close': 'last', 'volume': 'sum'}

        columns.remove('date')

        data_1H = data[columns]
        data_4H = data.resample('4H').agg(ohlc_dict)[columns]
        data_1D = data.resample('1D').agg(ohlc_dict)[columns]

        return {'1H': data_1H, '4H': data_4H, '1D': data_1D, 'ticker': coinPair}

    def getRateLimit(self):
        return self.exchange.rateLimit / 1000

    def getTickers(self):
        return self.exchange.fetchTickers()


def getBinanceDataFrame():
    binance = Exchange('binance')

    tickers = binance.getTickers()
    lx = []
    for ticker in tickers:
        tickerInfo = tickers[ticker]['info']
        tickerBase = ticker.split('/')
        if len(tickerBase) > 1:
            tickerInfo['baseCurrency'] = tickerBase[1]
        lx.append(tickerInfo)

    cols = ['symbol', 'baseCurrency', 'lastPrice', 'lastQty',
            'openTime', 'prevClosePrice', 'priceChange',
            'priceChangePercent', 'quoteVolume',  'volume',
            'weightedAvgPrice']
    data = pd.DataFrame(lx)[cols].set_index('symbol')
    cols.remove('symbol')
    cols.remove('baseCurrency')

    base_currencies = [base for base in data.baseCurrency.unique()
                       if isinstance(base, str)]
    # print(base_currencies)
    d = {}
    for coin in base_currencies:
        d[coin] = data[data['baseCurrency'] == coin]
        d[coin] = d[coin][cols].apply(pd.to_numeric, errors='coerce')
    return d


def getBittrexDataFrame():
    binance = Exchange('bittrex')

    tickers = binance.getTickers()
    # print(tickers)
    lx = []
    for ticker in tickers:
        tickerInfo = tickers[ticker]['info']
        tickerBase = ticker.split('/')
        if len(tickerBase) > 1:
            tickerInfo['baseCurrency'] = tickerBase[1]
        lx.append(tickerInfo)
    # print(lx[0].keys())

    cols = ['MarketName', 'High', 'Low', 'Volume', 'Last', 'BaseVolume',
            'Bid', 'Ask', 'OpenBuyOrders', 'OpenSellOrders', 'PrevDay', 'baseCurrency']
    data = pd.DataFrame(lx)[cols].set_index('MarketName')
    cols.remove('MarketName')
    cols.remove('baseCurrency')

    base_currencies = [base for base in data.baseCurrency.unique()
                       if isinstance(base, str)]
    # print(base_currencies)
    d = {}
    for coin in base_currencies:
        d[coin] = data[data['baseCurrency'] == coin]
        d[coin] = d[coin][cols].apply(pd.to_numeric, errors='coerce')
    return d

# print(getBittrexDataFrame())
# {'1ST/BTC': {'symbol': '1ST/BTC', 'timestamp': 1520229285763, 'datetime': '2018-03-05T05:54:46.763Z', 'high': 3.599e-05, 'low': 2.991e-05, 'bid': 3.179e-05, 'ask': 3.198e-05, 'vwap': None, 'open': None, 'close': None, 'first': None, 'last': 3.198e-05, 'change': None, 'percentage': None, 'average': None, 'baseVolume': 2350712.34731166, 'quoteVolume': 76.49616257,
# 'info': {'MarketName': 'BTC-1ST', 'High': 3.599e-05, 'Low': 2.991e-05, 'Volume': 2350712.34731166, 'Last': 3.198e-05, 'BaseVolume': 76.49616257, 'TimeStamp': '2018-03-05T05:54:45.763', 'Bid': 3.179e-05, 'Ask': 3.198e-05, 'OpenBuyOrders': 221, 'OpenSellOrders': 2235, 'PrevDay': 3.152e-05, 'Created': '2017-06-06T01:22:35.727'}}
