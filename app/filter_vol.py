from exchange import getBinanceDataFrame
from time import sleep
import schedule
import datetime
from sound import play

VOLUME_MULTIPLIER = .15
QVOLUME_MULTIPLIER = 1.5
PCHG_MULTIPLIER = 2

LAST_PUMPED = []


def getBinancePumpers(baseCurrency, volume_multiplier=1, qvolume_mutliplier=1, pchg_multiplier=1):
    base = getBinanceDataFrame()[baseCurrency]
    base.sort_values(['priceChangePercent'], ascending=False, inplace=True)

    volume_threshold = base['volume'].mean() * volume_multiplier
    qvolume_threshold = base['quoteVolume'].mean() * qvolume_mutliplier
    pchg_threshold = base['priceChangePercent'].abs().mean() * pchg_multiplier

    # version 1
    # pumpers = base[base['volume'] > volume_threshold]
    pumpers = base[base['quoteVolume'] > qvolume_threshold]

    pumpers = pumpers[pumpers['priceChangePercent'] > pchg_threshold]

    print(datetime.datetime.today())

    print('vt:', volume_threshold)
    print('qt:', qvolume_threshold)
    print('pct_th:', pchg_threshold)
    print()
    print('vm', VOLUME_MULTIPLIER)
    print('qm', QVOLUME_MULTIPLIER)
    print('pctm:', PCHG_MULTIPLIER)
    return pumpers


def clearLastPumped():
    global LAST_PUMPED
    LAST_PUMPED = []
    print('Pump list cleared')
schedule.every().hour.at(':00').do(clearLastPumped)

while True:
    schedule.run_pending()

    df = getBinancePumpers('BTC', volume_multiplier=VOLUME_MULTIPLIER,
                           qvolume_mutliplier=QVOLUME_MULTIPLIER, pchg_multiplier=PCHG_MULTIPLIER)
    if len(df) < 2:
        # VOLUME_MULTIPLIER *= .25
        QVOLUME_MULTIPLIER *= .5
        # PCHG_MULTIPLIER *= .25
        print('List < 2.')
        print('Reducing volume multiplier.')
    if len(df) > 6:
        QVOLUME_MULTIPLIER /= .5
        # PCHG_MULTIPLIER /= .25
        print('List > 8.')
        print('Increasing volume multiplier.')

    col = ['priceChangePercent', 'quoteVolume', 'lastPrice']

    # print(df[col])
    # print()

    CURRENT_PUMPED = list(df.index)
    NEW_PUMPED = [coin for coin in CURRENT_PUMPED if coin not in LAST_PUMPED]
    LAST_PUMPED = list(set(CURRENT_PUMPED + LAST_PUMPED))
    for coin in NEW_PUMPED:
        x = coin.split('BTC')[0]
        page = 'https://www.binance.com/trade.html?symbol={}_BTC'.format(x)
        print('Watch:', coin, 'Page:', page)
        play()

    sleep(30)
