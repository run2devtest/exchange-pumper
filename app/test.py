from coinmarketcap import Market
import matplotlib.pyplot as plt
import pandas as pd
import scipy.stats as stats
from time import sleep
import numpy as np


while True:
    cmc = Market()

    assets = cmc.stats()['active_assets']
    currencies = cmc.stats()['active_currencies']
    assets_and_currencies = assets + currencies

    df = pd.DataFrame(cmc.ticker(limit=assets_and_currencies))
    df.set_index('id', inplace=True)

    num_col = ['24h_volume_usd', 'available_supply', 'market_cap_usd', 'max_supply', 'percent_change_1h',
               'percent_change_24h', 'percent_change_7d', 'price_btc', 'price_usd', 'rank', 'total_supply']

    df[num_col] = df[num_col].apply(pd.to_numeric, errors='coerce')
    bitcoin = df[df.index == 'bitcoin']
    bitcoin_price = bitcoin['price_usd'][0]

    df = df[df.index != 'bitcoin']
    df = df[df.index != 'tether']

    df = df[df['24h_volume_usd'] > bitcoin_price]
    df = df[df['percent_change_1h'] > 0]
    df.sort_values('24h_volume_usd', inplace=True)

    df['volume_zscore'] = stats.zscore(df['24h_volume_usd'])
    df['pct_1h_zscore'] = stats.zscore(df['percent_change_1h'])

    df = df[df['volume_zscore'] > 0]
    df = df[df['pct_1h_zscore'] > 0.5]

    # df[['percent_change_7d', 'percent_change_1h', 'percent_change_24h', 'volume_zscore']].plot(
    #     kind='bar', use_index=True)
    # # plt.show()

    print(df[['percent_change_1h', 'pct_1h_zscore',
              '24h_volume_usd', 'volume_zscore']])

    sleep(60 * 5)
